package dpp.lab3.reminder;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * A class that performs function of main and is also the main frame of the application.
 * This class is responsible for displaying soon to come birthdays and to give the user
 * access to other panels.
 * @author sterlecki226045
 *
 */
public class MainFrame extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JButton complimentsButton = new JButton("Życzenia");
		complimentsButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { /*Show compliments panel*/ }
		});
		complimentsButton.setBounds(0, 134, 169, 134);
		getContentPane().add(complimentsButton);
		
		JButton usersButton = new JButton("Użytkownicy");
		usersButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) { /*Show users panel*/ }
		});
		usersButton.setBounds(0, 0, 169, 134);
		getContentPane().add(usersButton);

	}
}
