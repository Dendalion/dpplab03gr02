package dpp.lab3.reminder.panels;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;

public class ContactsPanel extends JPanel {
	private JTextField nameTextField;
	private JTextField surnameTextField;
	private JTextField birthDateTextField;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Create the panel.
	 */
	public ContactsPanel() {
		setLayout(null);
		
		JLabel contactDataLabel = new JLabel("Dane kontaktowe");
		contactDataLabel.setFont(new Font("Dialog", Font.BOLD, 15));
		contactDataLabel.setBounds(145, 12, 145, 15);
		add(contactDataLabel);
		
		JLabel chooseUserLabel = new JLabel("Wybierz użytkownika:");
		chooseUserLabel.setBounds(55, 54, 167, 15);
		add(chooseUserLabel);
		
		JComboBox userComboBox = new JComboBox();
		userComboBox.setBounds(240, 51, 155, 20);
		add(userComboBox);
		
		JLabel nameLabel = new JLabel("Imię:");
		nameLabel.setBounds(106, 92, 70, 15);
		add(nameLabel);
		
		JLabel surnameLabel = new JLabel("Nazwisko:");
		surnameLabel.setBounds(106, 119, 82, 15);
		add(surnameLabel);
		
		JLabel birthDateLabel = new JLabel("Data ur.:");
		birthDateLabel.setBounds(106, 146, 93, 15);
		add(birthDateLabel);
		
		JLabel emailLabel = new JLabel("E-mail:");
		emailLabel.setBounds(106, 173, 70, 15);
		add(emailLabel);
		
		JLabel phoneNumberLabel = new JLabel("Tel.:");
		phoneNumberLabel.setBounds(106, 200, 70, 15);
		add(phoneNumberLabel);
		
		nameTextField = new JTextField();
		nameTextField.setBounds(240, 90, 114, 19);
		add(nameTextField);
		nameTextField.setColumns(10);
		
		surnameTextField = new JTextField();
		surnameTextField.setBounds(240, 144, 114, 19);
		add(surnameTextField);
		surnameTextField.setColumns(10);
		
		birthDateTextField = new JTextField();
		birthDateTextField.setBounds(240, 117, 114, 19);
		add(birthDateTextField);
		birthDateTextField.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(240, 171, 114, 19);
		add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(240, 200, 114, 19);
		add(textField_4);
		textField_4.setColumns(10);
		
		JButton editButton = new JButton("Edytuj");
		editButton.setBounds(106, 227, 117, 25);
		add(editButton);
		
		JButton saveButton = new JButton("Zapisz");
		saveButton.setBounds(237, 227, 117, 25);
		add(saveButton);
		
		JButton backButton = new JButton("Powrót");
		backButton.setBounds(174, 264, 117, 25);
		add(backButton);

	}
}
