package dpp.lab3.reminder.user;

import java.util.ArrayList;
import java.util.List;

import dpp.lab3.reminder.compliments.Compliments;

public class User {
	

	private String _Imię;
	private String _Nazwisko;
	private String _DataUr;
	private String _Email;
	private int _Telefon;
	private int _UserID;
	/*
	 * Lista użytkowników, którzy są kontaktami danego użytkownika
	 */
	public List<User> _Kontakty = new ArrayList<User>();
	/*
	 * List życzeń
	 */
	public List<Compliments> _Zyczenia = new ArrayList<Compliments>();
	
	/*
	 * Pusty konstruktor
	 */
	public User() {
		
	}
	
	public void UserFill(String[] data, int Tel, int User) {
		this._Imię = data[0];
		this._Imię = data[1];
		this._Imię = data[2];
		this._Imię = data[3];
		this._Telefon = Tel;
		this._UserID = User;
	}
	
	public String getImię() {
		return _Imię;
	}
	
	public void setImię(String imię) {
		_Imię = imię;
	}
	
	public String getNazwisko() {
		return _Nazwisko;
	}
	
	public void setNazwisko(String nazwisko) {
		_Nazwisko = nazwisko;
	}
	
	public String getDataUr() {
		return _DataUr;
	}
	
	public void setDataUr(String dataUr) {
		_DataUr = dataUr;
	}
	
	public String getEmail() {
		return _Email;
	}
	
	public void setEmail(String email) {
		_Email = email;
	}
	
	public int getTelefon() {
		return _Telefon;
	}
	
	public void setTelefon(int telefon) {
		_Telefon = telefon;
	}
	
	public int getUserID() {
		return _UserID;
	}
	
	public void setUserID(int userID) {
		_UserID = userID;
	}

	public List<User> getKontakty() {
		return _Kontakty;
	}

	public void setKontakty(List<User> kontakty) {
		_Kontakty = kontakty;
	}

	public List<Compliments> getŻyczenia() {
		return _Zyczenia;
	}

	public void setŻyczenia(List<Compliments> życzenia) {
		_Zyczenia = życzenia;
	}

	/*
	 * Funkcja equals
	 * 
	 * @param obj obiekt do którego porównujemy aktualny obiekt
	 * 
	 * @return zwraca true jeśli obiekty są tym samym obiektem 
	 * / false jeśli się różnią
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (_UserID != other._UserID)
			return false;
		return true;
	}
	

}
