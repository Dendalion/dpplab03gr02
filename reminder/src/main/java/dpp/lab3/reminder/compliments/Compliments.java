package dpp.lab3.reminder.compliments;

public class Compliments {
	
	protected String _Adresat;
	protected String _Autor;
	protected String _Tresc;
	protected int _ComplimentsID;
	
	public Compliments() {
		
	}
	
	public void ComplimentsFill(String[] data, int complimentsID) {
		this._Adresat = data[0];
		this._Autor = data[1];
		this._Tresc = data[2];
		this._ComplimentsID = complimentsID;
	}
	
	public String getAdresat() {
		return _Adresat;
	}
	public void setAdresat(String adresat) {
		_Adresat = adresat;
	}
	public String getAutor() {
		return _Autor;
	}
	public void setAutor(String autor) {
		_Autor = autor;
	}
	public String getTreść() {
		return _Tresc;
	}
	public void setTreść(String treść) {
		_Tresc = treść;
	}
	public int getComplimentsID() {
		return _ComplimentsID;
	}
	public void setComplimentsID(int complimentsID) {
		_ComplimentsID = complimentsID;
	}

	
}
